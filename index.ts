import MerkleDistributor, { UserResponse } from '@jup-ag/merkle-distributor-sdk';
import { Keypair, Connection, Transaction, PublicKey, TransactionInstruction, SystemProgram, LAMPORTS_PER_SOL, AccountInfo } from "@solana/web3.js";
import { AnchorProvider, Wallet } from '@coral-xyz/anchor';
import { TOKEN_PROGRAM_ID, ASSOCIATED_TOKEN_PROGRAM_ID, getAssociatedTokenAddress, createAssociatedTokenAccountInstruction, createTransferInstruction, Mint } from '@solana/spl-token';

import * as bs58 from 'bs58';
import * as dotenv from 'dotenv';
dotenv.config();

const privateKeyFromCompWallet: Array<string> = [
   // comp wallet private key #1 (as a string)
   // Add here as many compromised you have following the sae format
  `${process.env.WALLET_COMP_1}`,
  // `${process.env.WALLET_COMP_2}`,
  // `${process.env.WALLET_COMP_...}`,
];

const jupTokenAddress: PublicKey = new PublicKey("JUPyiwrYJFskUPiHa7hkeR8VUtAeFoSYbKedZNsDvCN");


// Wallet who pay the fees and get back alloc JUP
const privateKeyWallet: string = `${process.env.WALLET_NOT_COMP}`;
const wallet: Keypair = Keypair.fromSecretKey(bs58.decode(privateKeyWallet));

// Initialize connection and wallet
const connection: Connection = new Connection(`https://mainnet.helius-rpc.com/?api-key=${process.env.HELIUS_API_KEY}`);


async function getAssociatedTokenAccount(mint: PublicKey, owner: PublicKey): Promise<PublicKey> {

  const tokenAccount = await getAssociatedTokenAddress(
    mint,
    owner,  // to
    true,
    TOKEN_PROGRAM_ID,
    ASSOCIATED_TOKEN_PROGRAM_ID,
  );

  return tokenAccount;
}

async function createAssociatedTokenAccount(payer: PublicKey, associatedTokenAccount: PublicKey, owner: PublicKey): Promise<TransactionInstruction> {

  const createAtaInstruction = createAssociatedTokenAccountInstruction(
    payer, // payer
    associatedTokenAccount, // destination
    owner, // owner of destination
    jupTokenAddress,
    TOKEN_PROGRAM_ID,
    ASSOCIATED_TOKEN_PROGRAM_ID
  )

  return createAtaInstruction;
}


async function getAllocFrom(publicKey: PublicKey, merkleDistributor: MerkleDistributor) {
  try {
    const alloc: UserResponse = await merkleDistributor.getUser(publicKey);
    console.log("Alloc size : " + alloc.amount);

    if (!alloc) {
      return;
    }

    return alloc;

  } catch (err) {
    console.log(err);

    throw err;
  }
}

async function main() {

  
  const currentSlot: number = await connection.getSlot();

  if (currentSlot < 245286497) {

    console.log('currentSlot is ' + currentSlot);
    console.log('start slot is 245286497');

    var remaingingSlots: number = 245286497 - currentSlot;

    console.log('Remaining slots : ' + remaingingSlots);

    let ms: number = 0;

    if (remaingingSlots > 1000)
      ms = 3000;
    else if (remaingingSlots > 500)
      ms = 1000;
    else if (remaingingSlots > 50)
      ms = 500;

    console.log('We try again in ' + ms + ' ms');

    setTimeout(main, ms);

    return;
  }

  for (let privateKey of privateKeyFromCompWallet) {
    try {

      const compKeypair: Keypair = Keypair.fromSecretKey(bs58.decode(privateKey));
      const compWallet: Wallet = new Wallet(compKeypair);

      const provider = new AnchorProvider(connection, compWallet, {
        commitment: 'confirmed',
      });

      // Initialize MerkleDistributor
      const merkleDistributor: MerkleDistributor = new MerkleDistributor(provider, {
        targetToken: jupTokenAddress,
        claimProofEndpoint: 'https://worker.jup.ag/jup-claim-proof',
      });

      // Check if user has allocations

      console.log("trying to claim for this wallet : "+compKeypair.publicKey.toString());

      const alloc = await getAllocFrom(new PublicKey(compKeypair.publicKey), merkleDistributor);

      if (!alloc || alloc.amount === 0) {
        console.log('No Alloc ! You fucked up, bozo !!');
        console.log('alloc', alloc)
          continue;
      };

      // Create tx
      let tx = new Transaction();

      // Add instructions for sending sol to allocWallet from good wallet
      tx.add(SystemProgram.transfer({
        // Account that will send transferred lamports
        fromPubkey: <PublicKey>wallet.publicKey,
        // Account that will receive transferred lamports
        toPubkey: <PublicKey>compKeypair.publicKey,
        // Minimum amount of lamports to transfer for recover all fees
        lamports: LAMPORTS_PER_SOL * 0.07,
      }));

      // Get ATA from allocWallet and $JUP (created by Jupiter)
      let associatedJupCompTokenAccount: PublicKey = await getAssociatedTokenAccount(jupTokenAddress, compKeypair.publicKey);

      console.log('ATA wallet comp :' + associatedJupCompTokenAccount);

      // Get claim instructions
      const ixs: TransactionInstruction[] = (await merkleDistributor.claimToken(compKeypair.publicKey)) || [];

      if (ixs.length === 0) {
        console.error('No instructions to claim.');
        return;
      }

      // Add Merkle Instructions at the tx
      tx.add(...ixs);

      // Create ATA for wallet who get back $JUP
      let associatedJupTokenAccountWallet: PublicKey = await getAssociatedTokenAccount(jupTokenAddress, wallet.publicKey);

      console.log('ATA wallet not comp :' + associatedJupTokenAccountWallet);

      // Check if account already exist, if not (null), create it and add it to the transaction
      const dataTmp2 = await connection.getAccountInfo(associatedJupTokenAccountWallet);

      if (dataTmp2 === null) {

        const createAtaInstructionSecours: TransactionInstruction = await createAssociatedTokenAccount(wallet.publicKey, associatedJupTokenAccountWallet, wallet.publicKey);

        tx.add(createAtaInstructionSecours);
      }

      // Add $JUP transfert instruction to transaction
      tx.add(createTransferInstruction(
        associatedJupCompTokenAccount, // source
        associatedJupTokenAccountWallet, // destination
        compKeypair.publicKey, // payer
        alloc.amount
      ));

      const blockHash = (await connection.getLatestBlockhash('finalized')).blockhash;
      tx.feePayer = wallet.publicKey;
      tx.recentBlockhash = blockHash;

      tx.sign(wallet, compKeypair)

      const txnSignature: String = await connection.sendRawTransaction(
        tx.serialize(),
        // leave the flag at false to avoid sending the tx if an error occurs
        // Actually, Jupiter returns a ClaimingIsNotStarted error, which make sense ;)
        { skipPreflight: true }
      );

      console.log("Signature : " + txnSignature);

      // Tx failed, re run
      if (!txnSignature) {
        console.log('FAILED !!! please try to run it again');
      }

      console.log("Transaction was sent, go check on solscan if it succeeded : " + txnSignature)

    } catch (error) {
      console.log(error);
    }
  }


}

main();
