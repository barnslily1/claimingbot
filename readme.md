# READ ME

## How to run this script

First, you have to get your API key for the RPC from Helius:

Go to :
```
https://www.helius.dev/
```

Then : 

- Click to the "Start for free" button
- Connect your wallet (not the compromised one!!)
- In the side menu, click on 'API keys'
- In the modal that just show up, c/p your Key Id and past it in the .env file your will create based on the .env.example file

## Clone the repo

In one of your directory run :

```
git clone https://gitlab.com/jondel/claimingbot.git
```

Then, move into the new directory with : 

```
cd claimingbot
```


In your root directory, create your .env file :
```
touch .env
```

and c/p the .env.example file content.

Update the file with your data.


Then, you have to install on your machine :
```
Node => version v18.17.1
Typescript
```

Install dependencies
```
npm install
```

Run
```
npx nodemon index.ts
```

# DISCLAIMER

There is no guarantee that this script will work as we cannot test it under real conditions!!

This script is obviously not perfect, so please bear with us.
If you think you can improve it, don't hesitate to suggest a PR.

In any case, THAT'S WORK ON MY MACHINE!



## SUPPORT

Feel free to support us : 
```
H3WQnCHKGVcaRspfqF7FPCmbLpC8yAxBJkVptUNCxra1
```

or by following us on X :

@fredericbry
@Le___Belge



# LICENCE

Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg