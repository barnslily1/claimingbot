"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var merkle_distributor_sdk_1 = require("@jup-ag/merkle-distributor-sdk");
var web3_js_1 = require("@solana/web3.js");
var anchor_1 = require("@coral-xyz/anchor");
var spl_token_1 = require("@solana/spl-token");
var bs58 = require("bs58");
var dotenv = require("dotenv");
dotenv.config();
var jupTokenAddress = new web3_js_1.PublicKey("JUPyiwrYJFskUPiHa7hkeR8VUtAeFoSYbKedZNsDvCN");
var jupClaimed = "".concat(process.env.JUP_TO_CLAIM);
// We don't know why but that doesn't work with 6 decimals as the JUP token account say it !!!
var jupTokenDecimals = 9;
// wallet where there are the alloc JUP
var privateKeyFromCompWallet = "".concat(process.env.KEY_1).concat(process.env.KEY_2);
var compKeypair = web3_js_1.Keypair.fromSecretKey(bs58.decode(privateKeyFromCompWallet));
var compWallet = new anchor_1.Wallet(web3_js_1.Keypair.fromSecretKey(bs58.decode(privateKeyFromCompWallet)));
console.log(compKeypair.publicKey.toString());
// Wallet who pay the fees and get back alloc JUP
var privateKeyWallet = "".concat(process.env.KEY_3).concat(process.env.KEY_4);
var wallet = web3_js_1.Keypair.fromSecretKey(bs58.decode(privateKeyWallet));
console.log(wallet.publicKey.toString());
// Initialize connection and wallet
var connection = new web3_js_1.Connection("https://mainnet.helius-rpc.com/?api-key=".concat(process.env.HELIUS_API_KEY));
var provider = new anchor_1.AnchorProvider(connection, compWallet, {
    commitment: 'confirmed',
});
// Initialize MerkleDistributor
var merkleDistributor = new merkle_distributor_sdk_1.default(provider, {
    targetToken: jupTokenAddress,
    claimProofEndpoint: 'https://worker.jup.ag/jup-claim-proof',
});
function getAssociatedTokenAccount(mint, owner) {
    return __awaiter(this, void 0, void 0, function () {
        var tokenAccount;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, (0, spl_token_1.getAssociatedTokenAddress)(mint, owner, // to
                    true, spl_token_1.TOKEN_PROGRAM_ID, spl_token_1.ASSOCIATED_TOKEN_PROGRAM_ID)];
                case 1:
                    tokenAccount = _a.sent();
                    return [2 /*return*/, tokenAccount];
            }
        });
    });
}
function createAssociatedTokenAccount(payer, associatedTokenAccount, owner) {
    return __awaiter(this, void 0, void 0, function () {
        var createAtaInstruction;
        return __generator(this, function (_a) {
            createAtaInstruction = (0, spl_token_1.createAssociatedTokenAccountInstruction)(payer, // payer
            associatedTokenAccount, // destination
            owner, // owner of destination
            jupTokenAddress, spl_token_1.TOKEN_PROGRAM_ID, spl_token_1.ASSOCIATED_TOKEN_PROGRAM_ID);
            return [2 /*return*/, createAtaInstruction];
        });
    });
}
function signAndSendTransactions(encodedTransaction) {
    return __awaiter(this, void 0, void 0, function () {
        var feePayer, feePayer_2, recoveredTransaction, txnSignature, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    feePayer = web3_js_1.Keypair.fromSecretKey(bs58.decode(privateKeyWallet));
                    feePayer_2 = web3_js_1.Keypair.fromSecretKey(bs58.decode(privateKeyFromCompWallet));
                    recoveredTransaction = web3_js_1.Transaction.from(Buffer.from(encodedTransaction, 'base64'));
                    recoveredTransaction.partialSign(feePayer, feePayer_2);
                    return [4 /*yield*/, connection.sendRawTransaction(recoveredTransaction.serialize(), 
                        // leave the flag at false to avoid sending the tx if an error occurs
                        // Actually, Jupiter returns a ClaimingIsNotStarted error, which make sense ;)
                        { skipPreflight: true })];
                case 1:
                    txnSignature = _a.sent();
                    console.log("Signature : " + txnSignature);
                    return [2 /*return*/, txnSignature];
                case 2:
                    error_1 = _a.sent();
                    console.log(error_1);
                    throw error_1;
                case 3: return [2 /*return*/];
            }
        });
    });
}
function getAllocFrom(publicKey) {
    return __awaiter(this, void 0, void 0, function () {
        var alloc, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, merkleDistributor.getUser(publicKey)];
                case 1:
                    alloc = _a.sent();
                    console.log("Alloc size : " + alloc.amount);
                    if (!alloc) {
                        return [2 /*return*/];
                    }
                    return [2 /*return*/, alloc];
                case 2:
                    err_1 = _a.sent();
                    console.log(err_1);
                    throw err_1;
                case 3: return [2 /*return*/];
            }
        });
    });
}
function main() {
    return __awaiter(this, void 0, void 0, function () {
        var alloc, tx, associatedJupCompTokenAccount, ixs, associatedJupTokenAccountWallet, dataTmp2, createAtaInstructionSecours, blockHash, serializedTransaction, transactionBase64, signature, error_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 10, , 11]);
                    return [4 /*yield*/, getAllocFrom(new web3_js_1.PublicKey(compKeypair.publicKey))];
                case 1:
                    alloc = _a.sent();
                    if (!alloc || alloc.amount === 0) {
                        console.log('No Alloc ! You fucked up, bozo !!');
                        return [2 /*return*/];
                    }
                    ;
                    tx = new web3_js_1.Transaction();
                    // Add instructions for sending sol to allocWallet
                    // from walletFromMe vers walletFromSon
                    tx.add(web3_js_1.SystemProgram.transfer({
                        // Account that will send transferred lamports
                        fromPubkey: wallet.publicKey,
                        // Account that will receive transferred lamports
                        toPubkey: compKeypair.publicKey,
                        // Minimum amount of lamports to transfer for recover all fees
                        lamports: web3_js_1.LAMPORTS_PER_SOL * 0.07,
                    }));
                    return [4 /*yield*/, getAssociatedTokenAccount(jupTokenAddress, compKeypair.publicKey)];
                case 2:
                    associatedJupCompTokenAccount = _a.sent();
                    return [4 /*yield*/, merkleDistributor.claimToken(compKeypair.publicKey)];
                case 3:
                    ixs = (_a.sent()) || [];
                    if (ixs.length === 0) {
                        console.error('No instructions to claim.');
                        return [2 /*return*/];
                    }
                    // Add Merkle Instructions at the tx
                    tx.add.apply(tx, ixs);
                    return [4 /*yield*/, getAssociatedTokenAccount(jupTokenAddress, wallet.publicKey)];
                case 4:
                    associatedJupTokenAccountWallet = _a.sent();
                    return [4 /*yield*/, connection.getAccountInfo(associatedJupTokenAccountWallet)];
                case 5:
                    dataTmp2 = _a.sent();
                    if (!(dataTmp2 === null)) return [3 /*break*/, 7];
                    return [4 /*yield*/, createAssociatedTokenAccount(wallet.publicKey, associatedJupTokenAccountWallet, wallet.publicKey)];
                case 6:
                    createAtaInstructionSecours = _a.sent();
                    tx.add(createAtaInstructionSecours);
                    _a.label = 7;
                case 7:
                    // Add $JUP transfert instruction to transaction
                    tx.add((0, spl_token_1.createTransferInstruction)(associatedJupCompTokenAccount, // source
                    associatedJupTokenAccountWallet, // destination
                    wallet.publicKey, // payer
                    parseInt(jupClaimed) * Math.pow(10, jupTokenDecimals)));
                    return [4 /*yield*/, connection.getLatestBlockhash('finalized')];
                case 8:
                    blockHash = (_a.sent()).blockhash;
                    tx.feePayer = wallet.publicKey;
                    tx.recentBlockhash = blockHash;
                    serializedTransaction = tx.serialize({ requireAllSignatures: false, verifySignatures: true });
                    transactionBase64 = serializedTransaction.toString('base64');
                    return [4 /*yield*/, signAndSendTransactions(transactionBase64)];
                case 9:
                    signature = _a.sent();
                    // If signature failed, re run
                    if (!signature) {
                        console.log('FAILED !!!');
                        main();
                    }
                    return [3 /*break*/, 11];
                case 10:
                    error_2 = _a.sent();
                    console.log(error_2);
                    return [3 /*break*/, 11];
                case 11: return [2 /*return*/];
            }
        });
    });
}
main();
